# final react project

START

1. Клонируем проект
2. Переключаем ветку на develop
3. Делеам yarn install (npm install) в корне проекта (backend)
4. Затем заходим в папку client и делаем yarn install (npm install) (frontend)
5. Наш реакт лежит в папке client и работаем мы только в ней.
6. Красиво делаем свою ветку от ветки develop в формате (feature/<номер и название вашей задачи>)

WORK

1. Запускаем сборку и выполняем задачу в ветке под эту задачу (feature/<номер и название вашей задачи>)
2. Останавливаем сборку (ctrl + c)
3. Мерджим свою ветку feature/<номер и название вашей задачи> в ветку develop
4. Переключаемся в ветку develop
5. Делаем git pull
6. Запускаем еще раз сборку на ветке develop, проверяем чтоб все работало так же как на вашей ветке с задачей.
7. Делаем git push
8. Заходим в gitlab в раздел CI/CD - pipelines, чекаем как у нас прошли этам загрузки на репозиторий
9. Чекаем наш сайт на gitlab.
